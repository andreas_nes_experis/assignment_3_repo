# Assignment nr. 3: Create a Web API and Database with Spring

## The latest project by
Andreas Kvernhaug and Andreas Nes

## Chapter 1
«Only one more annotation, I SWEAR!» he cried.

«PLEASE don't go.»

«This is why we can't be together» she said, opening the drawer under where the microwave should (but wasn't) and taking out some keys.

«Why is it so damn important to organize all your movies into some weird-ass browser-accessible database?»

«And in PostgreSQL, for crying out loud! Can't you just use Excel, like normal people?»

«You wouldn't understand» he said, wiping tears off of his face.

«I've told you before. Domain Driven Design is a metaphor for LIFE!»

She pulled her slightly worn out striped scarf from the oversized brown hand bag placed in the chair next to her husband, and wrapped it around her neck.

«We'll if it's such a good metaphor how come you didn't model me leaving you right now!»

«I did. Well, I tried. But something got screwed up in the configuration files I think... And, funny story, I tried calling a *repository* from, I'm not kidding you, *a service*! Exposing the internal business logic to the user in the process! What was I thinking?... And then... And then!»

«What happened to you...?» she whispered bleakly, with a look of despair on her face.

«I wish you luck in your projects. The kids... hope you will give them a call, every once in a while».

Her expression changed, gradually turning to one of anger. She was about to speak some real truth to the neckbeard-touting man as the doorbell rang. (No bombs were dropped.)

«Ding dong!».

«DING DONG DING DONG DING DONG DING DONG!».

«LET ME IN!». Furious knocking ensued.

The couple in the kitchen looked at each other with shock as they heard the door being knocked down, and a man running in.

«THIS IS PROFESSOR NICK, FROM THE FUTURE!», he said, panting.

Catching his breath, he continued: «DO NOT - I REPEAT, DO NOT, put that project in a Docker Container, don't you DARE touch that button!»

«Michael gave me your address. Wait, you don't know who that is yet. Well, you do, but... I'll explain later.»

«And thank goodness you didn't submit yet, I had to travel ALL THIS WAY just to tell you!»

«From... the future?» the middle-aged lady inquired, incredulous, and still in shock.

«Well that too. I mean from South Africa. Phew! What a journey that was! You wouldn't believe the traffic to the airport, it's crazy... And the flight was delayed... Chaos!»


- EDITORS NOTE -
Work hours are over and it's friday and time to party so you'll have to catch the next chapter some other time jk not gonna keep writing on this but thx 4 reading and have a nice week :)
